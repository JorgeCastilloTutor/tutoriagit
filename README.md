# TutoriaGit

### Introducción

Repositorio para la tutoría de Git y GitLab/GitHub.

### Integrantes
- Jorge
- Anngy
- Carlos
- Maria

Grupo Software S 22/44/45/57/58/67

Equipo de Trabajo 1-10

### Código

En la ***línea 123 del*** archivo **principal.py encontramos la** instrucción `print("Hola mundo")` señalamos la impresión en la *consola*.


`
print("Hola Mundo")

import pandas as pd

import numpy as np
`