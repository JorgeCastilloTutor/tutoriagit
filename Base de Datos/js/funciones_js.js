// Javascript

function botonBasica(){
    if(document.getElementById("calcBasica").style.display == "none"){
    	document.getElementById("calcBasica").style.display = "block";
    	document.getElementById("calcGraficadora").style.display = "none";
    }else{
        document.getElementById("calcBasica").style.display = "none";
    	document.getElementById("calcGraficadora").style.display = "none";
    }
}

function botonGrafica(){
    if(document.getElementById("calcGraficadora").style.display == "none"){
    	document.getElementById("calcBasica").style.display = "none";
    	document.getElementById("calcGraficadora").style.display = "block";
    }else{
        document.getElementById("calcBasica").style.display = "none";
    	document.getElementById("calcGraficadora").style.display = "none";
    }
}

const CHART_COLORS = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  lime: 'rgb(0, 255, 0)',
  yellow: 'rgb(255, 205, 86)',
  cyan: 'rgb(0, 255, 255)',
  silver: 'rgb(192, 192, 192)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  olive: 'rgb(128, 128, 0)',
  grey: 'rgb(201, 203, 207)'
};

const NAMED_COLORS = [
  CHART_COLORS.red,
  CHART_COLORS.orange,
  CHART_COLORS.lime,
  CHART_COLORS.yellow,
  CHART_COLORS.cyan,
  CHART_COLORS.silver,
  CHART_COLORS.green,
  CHART_COLORS.blue,
  CHART_COLORS.purple,
  CHART_COLORS.olive,
  CHART_COLORS.grey,
];